import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';
  public currentCount = 0;

  public decrementCounter() {
    this.currentCount--;
  }

  public resetCounter() {
    this.currentCount = 0;
  }

  public incrementCounter() {
    this.currentCount++;
  }
}
